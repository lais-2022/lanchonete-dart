import 'Bebida.dart';
import 'ItemCarrinho.dart';
import 'Lanche.dart';
import 'Lanchonete.dart';

void main() {
  var lanchonete = Lanchonete();
  var lanche =
      Lanche("X-Salada", 10.00, ['Pão', 'Alface', 'Hamburger', 'Tomate']);

  lanchonete.adicionarProduto(Bebida("Coca", 5.00, 2000));
  lanchonete.adicionarProduto(lanche);
  lanchonete.exibirMenu();

  ItemCarrinho item = ItemCarrinho(2, lanchonete.produtos[1]);

  lanchonete.adicionarProdutoCarrinho(item);
  //esta forma tambem é feita de mesma forma igual ao de cima citado
  lanchonete.adicionarProdutoCarrinho(ItemCarrinho(3, lanchonete.produtos[0]));

  lanchonete.removerProdutoCarrinho(lanchonete.carrinho[1]);


  lanchonete.atualizarProdutoCarrinho(lanchonete.carrinho[0], 5);
  lanchonete.exibirCarrinho();
}
