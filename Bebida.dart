import 'Produto.dart';
import 'TipoProduto.dart';

class Bebida extends Produto{
  int volume;
  Bebida (String nome, double preco, this.volume) :
  super(nome, preco, TipoProduto.Bebida);

  @override
  void exibirDetalhes(){
    super.exibirDetalhes();
    print('Volume:$volume ml');
  }
}