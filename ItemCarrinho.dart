import 'Produto.dart';

class ItemCarrinho extends Produto{
  int quantidade;

  ItemCarrinho(this.quantidade, Produto produto) : 
  super(produto.nome, produto.preco, produto.tipo);
}