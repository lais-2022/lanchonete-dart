import 'Produto.dart';
import 'ItemCarrinho.dart';

class Lanchonete {
  List<Produto> produtos = [];
  List<ItemCarrinho> carrinho = [];

  void adicionarProduto(Produto produto) {
    produtos.add(produto);
  }

  void adicionarProdutoCarrinho(ItemCarrinho itemCarrinho) {
    carrinho.add(itemCarrinho);
  }

  void removerProdutoCarrinho(ItemCarrinho itemCarrinho) {
    carrinho.remove(itemCarrinho);
  }

  atualizarProdutoCarrinho(ItemCarrinho itemCarrinho, int novaQuantidade) {
    final indice = carrinho.indexOf(itemCarrinho);
    if (indice >= 0) {
      carrinho[indice].quantidade = novaQuantidade;
    }
  }

  void exibirMenu() {
    print("=======Menu======");
    for (var produto in produtos) {
      produto.exibirDetalhes();
      print("Tipo:${produto.tipo.name}");
      print("");
    }
  }

  void exibirCarrinho() {
    print("===========CARRINHO=========");
    for (var itemCarrinho in carrinho) {
      print("Produto: ${itemCarrinho.nome}\n"
          "Qtdd: ${itemCarrinho.quantidade}\n"
          "Preço Unitário: ${itemCarrinho.preco}\n"
          "Total: ${itemCarrinho.preco * itemCarrinho.quantidade}\n");
    }
    print('Total da Compra: ${totalCarrinho()}');
  }

  // double totalCarrinho() {
  // double total = 0;
  //   for (var itemCarrinho in carrinho) {
  //     total +=itemCarrinho.preco * itemCarrinho.quantidade;
  //   }
  //   return total;
  // }

  double totalCarrinho() => carrinho.fold(
      0,
      (total, itemCarrinho) =>
          total + itemCarrinho.preco * itemCarrinho.quantidade);
}
