import 'Produto.dart';
import 'TipoProduto.dart';

class Lanche extends Produto{
  List<String>ingredientes; 
  
  Lanche(String nome, double preco, this.ingredientes) :
  super(nome, preco, TipoProduto.Comida);

  @override
  void exibirDetalhes(){
    super.exibirDetalhes();
    print('Ingredientes:${ingredientes.join(', ')}');
  }

}